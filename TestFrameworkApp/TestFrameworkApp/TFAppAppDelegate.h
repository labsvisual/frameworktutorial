//
//  TFAppAppDelegate.h
//  TestFrameworkApp
//
//  Created by Shreyansh Pandey on 31/07/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TFAppAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
- (IBAction)btnTestClicked:(id)sender;

@end
