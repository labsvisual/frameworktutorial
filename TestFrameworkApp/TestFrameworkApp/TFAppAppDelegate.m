//
//  TFAppAppDelegate.m
//  TestFrameworkApp
//
//  Created by Shreyansh Pandey on 31/07/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "TFAppAppDelegate.h"
#import "MyTestFramework/MyTestFramework.h"

@implementation TFAppAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

- (IBAction)btnTestClicked:(id)sender {
    MyTestFramework *test = [[MyTestFramework alloc] init];
    [test TestItOut];
}
@end
