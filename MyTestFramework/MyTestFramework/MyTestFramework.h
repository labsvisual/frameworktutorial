//
//  MyTestFramework.h
//  MyTestFramework
//
//  Created by Shreyansh Pandey on 30/07/14.
//
//

#import <Foundation/Foundation.h>

@interface MyTestFramework : NSObject

/**
  * The function prints out the string "Hello People." to the console screen.
 */
- (void) TestItOut;

@end