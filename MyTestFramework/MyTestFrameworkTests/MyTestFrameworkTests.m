//
//  MyTestFrameworkTests.m
//  MyTestFrameworkTests
//
//  Created by Shreyansh Pandey on 30/07/14.
//
//

#import <XCTest/XCTest.h>

@interface MyTestFrameworkTests : XCTestCase

@end

@implementation MyTestFrameworkTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
